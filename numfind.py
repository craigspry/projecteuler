import numpy as np
from operator import mul
from operator import add

array = []
with open('nums2.txt', 'r') as f:
    for line in f:
        array.append([int(x) for x in line.split(' ')])
a = np.array(array)
#print numpy.hsplit(a,20)
#print numpy.vsplit(a,20)

#it = np.nditer(a, flags=['multi_index'])	
#while not it.finished:
#    print "%d <%s>" % (it[0], it.multi_index)
#    it.iternext()
compArr = []
i = 0
while i<15:
    compArr.append( np.diagonal(a,i))
    if i>0:
	    compArr.append( np.diagonal(a,i*-1))
    i = i + 1
	
a1= np.flipud(a)
i=0
while i<15:
    compArr.append( np.diagonal(a1,i))
    if i>0:
	    compArr.append( np.diagonal(a1,i*-1))
    i = i + 1
	
for s in np.rot90(a):
    compArr.append( s)
	
for s in a:
    compArr.append( s)
	
largestThingy = 0l
for s in compArr:
    i=3
    while i<len(s):
        thingy = reduce(mul, s[i-3:i+1], 1l)
        if thingy > largestThingy:
            print s[i-3:i+1]
            largestThingy = thingy
        i=1+i

print largestThingy