import Primes
import itertools

def findPrime():
	s = '123456789'
	while len(s) > 0:
		for x in itertools.permutations(s[::-1], len(s)):
			if Primes.isPrime(int("".join(x))):
				print "".join(x)
				return
		s = s[:-1]
		
findPrime()