import math

primes = set()
notPrimes = set()
def isPrimeLookup(n):
	if n in notPrimes:
		return False
	if n in primes:
		return True
	if isPrime(n):
		primes.add(n)
		return True
	else:
		notPrimes.add(n)
		return False
	
	
def isPrime(n):
    if n<0:
        return False
    if n == 1:
        return False
    if n < 4:
        return True
    if n % 2 == 0:
        return False
    if n < 9:
        return True
    if n % 3 == 0:
        return False
    r = math.floor(n**0.5)
    f = 5
    while f <= r:
        if n % f == 0:
            return False
        if n % (f+2) == 0:
            return False
        f = f + 6
    return True
