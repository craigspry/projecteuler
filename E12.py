	
def factors(n):
    facts = []
    limit = int(n**0.5)+1
    sqr = limit -1
    for i in range(1,  limit):
        if n%i == 0:
	    facts.append(i)
	    if i != sqr:
	        facts.append(n/i)
    return facts
    
def naturalSeq(stop):
    n,m=1,1
    while m < stop:
        yield n
	m += 1
	n = n + m
		
for x in naturalSeq(100000):
    if x > 1000000:
        if len(factors(x)) > 500:
            print str(x) + ' ' + str(len(factors(x)))