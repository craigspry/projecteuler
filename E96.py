import numpy
import sudoku
import time
			
array = []
nums =[]
grid =''
tim = time.time()
with open('sudoku.txt', 'r') as f:
	for line in f:
		if line.find('Grid') > -1:
			if len(array) > 0:
				count = 0
				q,t= sudoku.solveSudoku( numpy.array(array))
				if len(q) > 0:
					nums.append(int( "".join(str(val) for val in q[0][:3])))
				array = []
			grid = line
		else:
			array.append([int(x) for x in list(line) if str(x).isdigit()])
q,t= sudoku.solveSudoku(numpy.array(array))
nums.append(int( "".join(str(val) for val in q[0][:3])))
print time.time() - tim, ' seconds '
print sum(nums)
