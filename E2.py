def fib():
    a, b = 1, 1
    while 1:
        yield a
        a, b = b, a + b
  
a = fib()
k = a.next()
sum = 0      
while k <= 4000000:
    if(k % 2 == 0):
        print k
        sum = k + sum
    k = a.next()
print sum
    