from itertools import *

def factors():
    for i in islice(count(), 2,600851475143 ** 0.5 + 1):
        if 600851475143%i ==0:
            yield i

def isPrime(n):
    for i in xrange(2,int(n ** 0.5) + 1):
        if n % i == 0:
             return False
    return True
		
for a in factors():
    if isPrime(a):
        print a