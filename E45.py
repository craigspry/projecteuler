import time

st = time.time()

arrp = [n*(3*n-1)/2 for n in range(1,1000000)]
pents = set(arrp)
pmax = max(arrp)
arrh = [n*(2*n-1) for n in range(1,1000000)]
hexs = set(arrh)
hmax = max(arrh)

def nextTriangle(start):
	i = start
	while i<hmax and i<pmax:
		yield i*((i+1)/2)
		i += 1
		
def findSolution(i):
	for n in nextTriangle(i):
		if n in pents and n in hexs:
			return n
			
print findSolution(286)

print time.time() - st, ' seconds'
#print pents.intersection(hexs)


			
