
def score(s):
	return sum([ord(c)-64 for c in s])
	
def indexScore(names):
	i = 1
	for name in names:
		yield score(name) * i
		i = i + 1

with open('names.txt', 'r') as f:
	s = f.read()
	s = s.replace('"', '')
	names = s.split(',')
	names.sort()
	print sum(indexScore(names))
	