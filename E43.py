import itertools

def testPerm(x):
	divisors=[2,3,5,7,11,13,17]
	for i in range(7,0,-1):
		if int(x[i:i+3])%divisors[i-1] != 0:
			return False
	return True



print testPerm('1409357286')
s = '1234567890'
tot=0
for x in itertools.permutations(s, len(s)):
	if testPerm("".join(x)):
		tot += int("".join(x))
		
print tot
	