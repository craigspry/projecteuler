import math
import itertools
import collections
import time
import numpy
import Primes

def perms(n):
    s = str(n)
    arr = filter(Primes.isPrimeLookup, (int(d) for d in filter(lambda x: x[0] != '0',[reduce(lambda a, b: a+b, y) for y in itertools.permutations(s, len(s))])))
    if len(arr) >= 3:
        return list(set(arr))
    return []


primes = filter(Primes.isPrimeLookup, range(1001, 10000, 2))

for p in primes:
    perm = perms(p)
    if len(perm) >= 3:
        lst = itertools.permutations(perm, 3)
        diffs = dict()
        for x in lst:
            if x[1]-x[0]!=0  and abs(x[0] - x[1]) == abs(x[1] - x[2]):
                print x, abs(x[0] - x[1])

 
