import Primes
import time

st = time.time()
primes = filter(Primes.isPrimeLookup, range(1,1001))

length = 0
amax=0
bmax=0
for a in range(-1000,1000):
    for b in primes:
        n = 0
        while Primes.isPrimeLookup(n**2+a*n+b):
            n += 1
        if n > length:
            amax,bmax,length = a,b,n
print 'Values ', amax,bmax,length
print 'Product ',amax*bmax
print time.time() - st, ' seconds'
