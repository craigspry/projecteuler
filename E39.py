import time

t = time.time()
squares = [x*x for x in xrange(1,1000)]
lookup = dict( zip(squares,range(1,1000)))

results = [0]*1000
for k in squares:
    for j in xrange(1, int(k**0.5)):
        ya=((k/j) - j)/2
        y = ya*ya 
        if y<k and y in lookup and (y+k) in lookup:
            p = ya + lookup[k] + lookup[(y+k)]
            if p<1000:
                results[p] += 1
                
print results.index(max(results))
print time.time() - t
            #if ya + lookup[k] + lookup[(y+k)] == 1000:
            #    print [ya, lookup[k] , lookup[(y+k)]]
            #    print ya * lookup[k] * lookup[(y+k)]
			
	