import math
import time
import Primes
 
primes =[2,3,5,7]
primes.extend(filter(Primes.isPrimeLookup, range(9,1000000, 2)))
chain=0
num=0
nums = []
 
k=1
while sum(primes[:k]) < 1000000:
    k+=1 
t = time.time() 
for i in range(0, len(primes[:k])):
    if len(nums) < len(primes[i:k]):
        for j in range(i, len(primes[:k])):
            x = sum(primes[i:j])
            if x in primes and j-i > chain:
                chain = j-i
                num = x
                nums = primes[i:j]
    else:
        break
print num
print time.time() - t
