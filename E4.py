import time

def findPel():
    answ = 0
    for i in range(999, 99, -1):
        for j in range(999, i, -1):
            k = i*j 
            s = str(k)
            if s == s[::-1]:
                if k > answ:
                    answ = k
    return answ


t = time.time()

print findPel()

print time.time() - t