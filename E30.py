import itertools
import math
import time


arr = [x**5 for x in range(0,10)]

lookup = dict( zip((str(a) for a in range(0,10)),arr))
rlookup = dict( zip(arr, range(0,10)))

def numFromArray(a):
	i=0
	rtn=0
	for x in a:
		rtn += rlookup[x]*(10**i)
		i += 1
	return rtn

#print sum(arr)
sums = []
prods= itertools.product(arr, repeat=6)
#print prods
for prod in prods:
	#print prod
	if sum(prod) == numFromArray(prod) and sum(prod) !=1:
		#print sum(prod)
		sums.append(sum(prod))
	
print sum(sums)


#print sum(filter(lambda i: sum (( lookup[x] for x in str(i))) == i, range(3,2540160)))






