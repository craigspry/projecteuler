import Primes
import time

def nextNum(n):
    while(n !=1):
        if n % 2 == 0:
            yield n
	    n = n/2
        else:
            yield n
	    n = (3*n) + 1
	
st = time.time()

q = 1000000
start = 0
longest = 0
while q > 1:
    if Primes.isPrimeLookup(q):
        x = nextNum(q)
        arr = [i for i in x]
        seq = len(arr)
        #print [seq, q, q%2, q%4]
        if seq > longest:
            longest,start = seq,q
    q = q -1
    
print [start,longest]
print time.time() - st, ' Seconds'
