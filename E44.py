import itertools
import time

st = time.time()

arr = [n*(3*n-1)/2 for n in range(1,10000)]
pents = set(arr)

sums = filter( lambda a : abs(a[1] - a[0])	in pents, filter(lambda a: abs(a[1] + a[0]) in pents, itertools.permutations(arr,2)))

if len(sums) > 0:
	print min([abs(x[1] - x[0]) for x in sums])
	
print time.time() - st, ' seconds'
