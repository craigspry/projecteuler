import itertools

list1 = filter(lambda a: a%10!=0, range(1,100))
list2 = filter(lambda a: a%10!=0, range(100, 10000))

nums = []
for x,y in itertools.product(list1,list2):
    z=x*y
    s="".join(sorted('%d%d%d'%(z,x,y)))
    if s == '123456789':
        nums.append(z)
        
print nums
print sum(nums)
print sum(set(nums))
    