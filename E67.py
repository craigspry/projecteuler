import time

#arr=[[75],
#[95,64],
#[17,47,82],
#[18,35,87,10],
#[20,04,82,47,65],
#[88,02,77,73,07,63,67],
#[99,65,04,28,06,16,70,92],
#[41,41,26,56,83,40,80,70,33],
#[41,48,72,33,47,32,37,16,94,29],
#[53,71,44,65,25,43,91,52,97,51,14],
#[70,11,33,28,77,73,17,78,39,68,17,57],
#[91,71,52,38,17,14,91,43,58,50,27,29,48],
#[63,66,04,68,89,53,67,30,73,16,69,87,40,31],
#[04,62,98,27,23,9,70,98,73,93,38,53,60,04,23]]

#total=0
#for a in arr:
#	print [duplicates(a, max(a)), max(a)]
#	total += a[0]
#print "total:%d" % total
#ndx = 0
#i=0
#total = 0
#while i< len(arr)-1:
#	print  [arr[i][ndx], ndx]
#	total += arr[i][ndx]
#	if i<len(arr) - 2:
#		if arr[i+1][ndx] < arr[i+1][ndx + 1]:
#			ndx = ndx + 1
#	i = i + 1
#print "total:%d" % total
arr = []
with open('triangle.txt', 'r') as f:
    for line in f:
        arr.append([int(x) for x in line.split(' ')])


i,j=1,0
start = time.time()
while i<len(arr):
    j = 0
    while j < len(arr[i]):
	    if j == 0:
		   arr[i][j] += arr[i-1][j]
	    elif j == len(arr[i])-1:
		    arr[i][j] += arr[i-1][j-1]
	    else:
		    arr[i][j] = max( arr[i][j] + arr[i-1][j-1] , arr[i][j] + arr[i-1][j])
		
	    j += 1
    i+=1
print "Running time", time.time() - start
print max(arr[-1])


	