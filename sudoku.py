import numpy



def with_index(seq):
    for i in xrange(len(seq)):
        for j in xrange(len(seq[i])):
            yield i, j, seq[i][j]
        
def maxSquare(i):
	if i<3:
		return 3
	if i<6:
		return 6
	else:
		return 9
		
	
def assignNumber(x,y, elemx, puzzlex):
	if len(elemx[(x,y)]) == 1:
		puzzlex[x,y] = elemx[(x,y)][0]
		elemx.pop((x,y))
		num = puzzlex[x,y]
		for i in range(10):
			if (x,i) in elemx:
				#check vertacle
				elemx[(x,i)] = filter(lambda a : a != num, elemx[(x,i)])
			if (i,y) in elemx:
				#check horizontal
				elemx[(i,y)] = filter(lambda a : a != num, elemx[(i,y)])
	    #check sourounding square
		for xs in range(maxSquare(x)-3, maxSquare(x)):
			for ys in range(maxSquare(y)-3, maxSquare(y)):
				if not(x==xs and y==ys) and (xs,ys) in elemx:
					elemx[(xs,ys)] = filter(lambda a : a != num, elemx[(xs,ys)])
		return True
	return False
				
def eleminate1(x,y, num, elemx, puzzlex):
    for i in range(10):
        if (x,i) in elemx:
			#check vertacle
            elemx[(x,i)] = filter(lambda a : a != num, elemx[(x,i)])
            assignNumber(x,i, elemx, puzzlex)
        if (i,y) in elemx:
			#check horizontal
            elemx[(i,y)] = filter(lambda a : a != num, elemx[(i,y)])
            assignNumber(i,y, elemx, puzzlex)
    #check sourounding square
    for xs in range(maxSquare(x)-3, maxSquare(x)):
		for ys in range(maxSquare(y)-3, maxSquare(y)):
			if not(x==xs and y==ys) and (xs,ys) in elemx:
				elemx[(xs,ys)] = filter(lambda a : a != num, elemx[(xs,ys)])
				assignNumber(xs,ys, elemx, puzzlex)
				

def eleminate2(x,y, num, elemx, puzzlex):
	if (x,y) not in elemx:
		return
	nums = set(elemx[(x,y)])
	axisnums = set()
	for i in range(9):
		if i!=y:
			if (x,i) in elemx:
				axisnums = axisnums.union(elemx[(x,i)])
			else:
				axisnums.add(puzzlex[x,i])
	diff = set(elemx[(x,y)]).difference(axisnums)
	lnums = list(diff)
	
	if len(lnums) == 1:
		puzzlex[x,y] = lnums[0]
		elemx.pop((x,y))
		for i in range(9):
			if (x,i) in elemx and i!=y:
				elemx[(x,i)] = filter(lambda a : a != puzzlex[x,y], elemx[(x,i)])
			if (i,y) in elemx and i!=x:
				elemx[(i,y)] = filter(lambda a : a != puzzlex[x,y], elemx[(i,y)])
		for xs in range(maxSquare(x)-3, maxSquare(x)):		
			for ys in range(maxSquare(y)-3, maxSquare(y)):
				if not(x==xs and y==ys) and (xs,ys) in elemx:
					elemx[(xs,ys)] = filter(lambda a : a != puzzlex[x,y], elemx[(xs,ys)])
		return
		
	nums = set(elemx[(x,y)])
	axisnums = set()
	for i in range(9):
		if i != x:
			if (i,y) in elemx:
				axisnums = axisnums.union(elemx[(i,y)])
			else:
				axisnums.add(puzzlex[i,y])
	diff = set(elemx[(x,y)]).difference(axisnums)
	lnums = list(diff)
	
	if len(lnums) == 1:
		puzzlex[x,y] = lnums[0]
		elemx.pop((x,y))
		for i in range(9):
			if (x,i) in elemx and i!=y:
				elemx[(x,i)] = filter(lambda a : a != puzzlex[x,y], elemx[(x,i)])
			if (i,y) in elemx and i!=x:
				elemx[(i,y)] = filter(lambda a : a != puzzlex[x,y], elemx[(i,y)])
		for xs in range(maxSquare(x)-3, maxSquare(x)):
			for ys in range(maxSquare(y)-3, maxSquare(y)):
				if not(x==xs and y==ys) and (xs,ys) in elemx:
					elemx[(xs,ys)] = filter(lambda a : a != puzzlex[x,y], elemx[(xs,ys)])
		return
		
	nums = set()
	for xs in range(maxSquare(x)-3, maxSquare(x)):		
		for ys in range(maxSquare(y)-3, maxSquare(y)):
			if not(x==xs and y==ys) and (xs,ys) in elemx:
				nums = nums.union(set(elemx[(xs,ys)]))
	lnums = list(set(elemx[(x,y)]).difference(nums))
	#print x,y, lnums
	if len(lnums) == 1:
		puzzlex[x,y] = lnums[0]
		elemx.pop((x,y))
		for i in range(9):
			if (x,i) in elemx and i!=y:
				elemx[(x,i)] = filter(lambda a : a != puzzlex[x,y], elemx[(x,i)])
			if (i,y) in elemx and i!=x:
				elemx[(i,y)] = filter(lambda a : a != puzzlex[x,y], elemx[(i,y)])
		return
		
def eleminate3(x,y, num, elemx, puzzlex):
	if not((x,y) in elemx) or len(elemx[(x,y)]) != 2:
		return
	foundx = False
	foundy = False
	founds = False
	for i in range(10):
		if (x,i) in elemx:
			#check vertacle
			if i!=y and elemx[(x,i)] == elemx[(x,y)]:
				foundx = True
		if (i,y) in elemx:
			#check horizontal
			if i!=x and elemx[(i,y)] == elemx[(x,y)]:
				foundy = True
	#check sourounding square
	for xs in range(maxSquare(x)-3, maxSquare(x)):
		for ys in range(maxSquare(y)-3, maxSquare(y)):
			if not(x==xs and y==ys) and (xs,ys) in elemx:
				if elemx[(xs,ys)] in elemx[(x,y)]:
					founds = True
	for i in range(10):
		if foundx and (x,i) in elemx and len(elemx[(x,i)]) >= len(elemx[(x,y)]) and elemx[(x,y)] != elemx[(x,i)]:
			elemx[(x,i)] = filter(lambda a : a not in elemx[(x,y)], elemx[(x,i)])
			assignNumber(x,i, elemx, puzzlex)
				
		if foundy and (i,y) in elemx and len(elemx[(i,y)]) >= len(elemx[(x,y)])and elemx[(x,y)] != elemx[(i,y)]:
			elemx[(i,y)] = filter(lambda a : a not in elemx[(x,y)], elemx[(i,y)])
			assignNumber(i,y, elemx, puzzlex)
				
	if founds:
		for xs in range(maxSquare(x)-3, maxSquare(x)):
			for ys in range(maxSquare(y)-3, maxSquare(y)):
				if not(x==xs and y==ys) and (xs,ys) in elemx and len(elemx[(xs,ys)]) >= len(elemx[(x,y)]) and elemx[(x,y)] != elemx[(xs,ys)]:
					elemx[(xs,ys)] = filter(lambda a : a not in elemx[(x,y)], elemx[(xs,ys)])
					assignNumber(xs,ys, elemx, puzzlex)
					
def eleminate4(x,y, elemx, puzzlex):
	if (x,y) in elemx and len(elemx[(x,y)]) != 3:
		return
	eset = set(elemx[(x,y)])
	xcount = 0
	ycount = 0
	scount = 0
	for i in range(9):
		if (x,i) in elemx:
			#check vertacle
			if i!=y and set(elemx[(x,i)]).issubset(eset):
				xcount += 1
		if (i,y) in elemx:
			#check horizontal
			if i!=x and (set(elemx[(i,y)]).issubset(eset) or elemx[(i,y)] == elemx[(x,y)]):
				ycount += 1
	#check sourounding square
	for xs in range(maxSquare(x)-3, maxSquare(x)):
		for ys in range(maxSquare(y)-3, maxSquare(y)):
			if not(x==xs and y==ys) and (xs,ys) in elemx:
				if set(elemx[(xs,ys)]).issubset(eset):
					scount += 1
	for i in range(9):
		if xcount ==2 and (x,i) in elemx and not set(elemx[(x,i)]).issubset(eset):
			elemx[(x,i)] = filter(lambda a : a not in elemx[(x,y)], elemx[(x,i)])
			assignNumber(x,i, elemx, puzzlex)
		if ycount == 2 and (i,y) in elemx and not set(elemx[(i,y)]).issubset(eset):
			elemx[(i,y)] = filter(lambda a : a not in elemx[(x,y)], elemx[(i,y)])
			assignNumber(i,y, elemx, puzzlex)
	if scount == 2:
		for xs in range(maxSquare(x)-3, maxSquare(x)):
			for ys in range(maxSquare(y)-3, maxSquare(y)):
				if not(x==xs and y==ys) and (xs,ys) in elemx and not set(elemx[(xs,ys)]).issubset(eset):
					elemx[(xs,ys)] = filter(lambda a : a not in elemx[(x,y)], elemx[(xs,ys)])
					assignNumber(xs,ys, elemx, puzzlex)
							
def eleminate5(x,y, elemx, puzzlex):
	numsx = dict()
	numsy = dict()
	for i in range(9):
		if (x,i) in elemx:
			for num in elemx[(x,i)]:
				if num not in numsx:
					numsx[num] = []
				numsx[num].append((x,i))
		if (i,y) in elemx:
			for num in elemx[(i,y)]:
				if num not in numsy:
					numsy[num] = []
				numsy[num].append((i,y))
	for key, value in numsx.items():
		if len(value) == 2 and maxSquare(value[0][0]) == maxSquare(value[1][0]) and maxSquare(value[0][1]) == maxSquare(value[1][1]):
			for xs in range(maxSquare(value[0][0])-3, maxSquare(value[0][0])):
				for ys in range(maxSquare(value[0][1])-3, maxSquare(value[0][1])):
					if (xs,ys) in elemx and xs!=value[0][0] and xs!=value[1][0] and ys!=value[0][1] and ys!=value[1][1]:
						elemx[(xs,ys)] = filter(lambda a : a != key, elemx[(xs,ys)]) 
						assignNumber(xs,ys,elemx,puzzlex)
	for key, value in numsy.items():
		if len(value) == 2 and maxSquare(value[0][0]) == maxSquare(value[1][0]) and maxSquare(value[0][1]) == maxSquare(value[1][1]):
			for xs in range(maxSquare(value[0][0])-3, maxSquare(value[0][0])):
				for ys in range(maxSquare(value[0][1])-3, maxSquare(value[0][1])):
					if (xs,ys) in elemx and xs!=value[0][0] and xs!=value[1][0] and ys!=value[0][1] and ys!=value[1][1]:
						elemx[(xs,ys)] = filter(lambda a : a != key, elemx[(xs,ys)]) 
						assignNumber(xs,ys,elemx,puzzlex)
	nums = dict()
	for xs in range(maxSquare(x)-3, maxSquare(x)):
		for ys in range(maxSquare(y)-3, maxSquare(y)):
			if (xs,ys) in elemx:
				for num in elemx[(xs,ys)]:
					if num not in nums:
						nums[num] = []
					nums[num].append((xs,ys))
	maxx = maxSquare(x)
	maxy = maxSquare(y)
	for key, value in nums.items():
		if len(value) == 2:
			if value[0][0] == value[1][0]:
				for i in range(9):
					if i < maxy-3 or i> maxy:
						if (value[0][0],i) in elemx and i!=y:
							elemx[(value[0][0],i)] = filter(lambda a : a != key, elemx[(value[0][0],i)]) 
							assignNumber(value[0][0],i,elemx,puzzlex)
							#return
			if value[0][1] == value[1][1]:
				for i in range(9):
					if i <maxx-3 or i > maxx:
						if (i,value[0][1]) in elemx and i!=x:
							elemx[(i,value[0][1])] = filter(lambda a : a != key, elemx[(i,value[0][1])]) 
							assignNumber(i,value[0][1],elemx,puzzlex)

def eleminate6(elemx, puzzlex):
	elist=[]
	nums = dict()
	twosx ={}
	for x in range(9):
		numsx ={}
		numsy ={}
		for y in range(9):
			if (x,y) in elemx:
				for num in elemx[(x,y)]:
					if num not in nums:
						nums[num] = []
					if num not in numsx:
						numsx[num]=[]
					nums[num].append((x,y))
					numsx[num].append((x,y))
		for key, value in sorted(numsx.items()):
			if len(value) == 2:
				if key not in twosx:
					twosx[key] = []
				twosx[key].append(value)
	for key, value in sorted(twosx.items()):
			for i,v in enumerate(value):
				for j,w in enumerate(value):
					if i!=j:
						if v[0][1] == w[0][1] and v[1][1] == w[1][1]:
							for g in range(9):
								if g!=v[0][0] and g!=w[0][0]:
									if (g, v[0][1]) in elemx:
										elemx[(g, v[0][1])] = filter(lambda a : a != key, elemx[(g, v[0][1])]) 
										assignNumber(g, v[0][1],elemx,puzzlex)
									if (g, v[1][1]) in elemx:
										elemx[(g, v[1][1])] = filter(lambda a : a != key, elemx[(g, v[1][1])]) 
										assignNumber(g, v[1][1],elemx,puzzlex)
				

def checkSolution(x,y, num, elemx, puzzlex):
	for i in range(9):
		if (x,i) not in elemx  and i!=y and puzzlex[x,i]== puzzlex[x,y]:
			print 'ERROR Y', x,y
			exit()
		if (i,y) not in elemx  and i!=x and puzzlex[i,y]== puzzlex[x,y]:
			print 'ERROR X', x,y, i,y
			exit()
	for xs in range(maxSquare(x)-3, maxSquare(x)):
		for ys in range(maxSquare(y)-3, maxSquare(y)):
			if not(x==xs and y==ys) and (xs,ys) not in elemx and puzzlex[xs,ys]== puzzlex[x,y]:
				print 'ERROR S',x,y
				exit()

			
def solveSudoku(puzzle):
	puzzlex = numpy.array(puzzle)
	z = numpy.where(puzzlex == 0)
	zeros = zip(z[0], z[1])
	elemx = dict()
	for z in zeros:
		elemx[z] = range(1,10)
	k = 1
	oldp = numpy.array(puzzlex)
	count = 0
	while len(elemx.keys()) > 0:
		for x,y,num in with_index(puzzlex):
			if num != 0:
				eleminate1(x,y,num, elemx, puzzlex)
		for x,y,num in with_index(puzzlex):
			if num == 0:
				eleminate2(x,y,num, elemx, puzzlex)
		for x,y,num in with_index(puzzlex):
			if num == 0:
				eleminate3(x,y,num, elemx, puzzlex)
		for x,y,num in with_index(puzzlex):
			if num == 0:
				eleminate4(x,y, elemx, puzzlex)
		for x,y,num in with_index(puzzlex):
			if num == 0:
				eleminate5(x,y, elemx, puzzlex)
		eleminate6(elemx, puzzlex)
		for x,y,num in with_index(puzzlex):
			checkSolution(x,y,num, elemx, puzzlex)
		#print 'pass ', k
		k += 1
		if (oldp == puzzlex).all():
			count += 1
			if count > 5:
				print puzzlex
				for key, value in sorted(elemx.items()):
					print key, value
				print "ERROR too many iterations"
				count = 0
				return [],-1
		else:
			count = 0
		oldp = numpy.array(puzzlex)
	return puzzlex,k
