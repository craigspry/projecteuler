import math
import itertools
import time
from Primes import isPrime

def rotateString(s):
    for i in range(1, len(s)+1):
        yield s[i:] + s[:i]


digits = [ '2', '4', '5', '6', '8']
cprimes = set([2,3])
def isCircle(n):
    if n<10:
        return True
    if n in cprimes:
        return True
    s = str(n)
    for c in digits:
        if c in s:
            return False
    arr = filter(isPrime, (int(d) for d in [num for num in rotateString(s)]))
    if len(arr) == len(s):
        cprimes.update(set(arr))
        return True
    return False

def primes(n):
    i=1
    while i<n:
        if isPrime(i):
            if isCircle(i):
                yield i
        i+=1

t = time.time()
print len([x for x in primes(1000000)])
print cprimes

print time.time() -t
