import itertools
import time

st = time.time()
print filter(lambda s: s == s[::-1], (str(w) for w in sorted((x*y for x,y in itertools.combinations_with_replacement(range(999,99,-1),2)), reverse=2)))[0]
print time.time() - st
