import Primes
import time

st = time.time()

def oddComps():
	i = 3
	while True:
		if not Primes.isPrimeLookup(i):
			yield i
		i+=2

for o in oddComps():
	i=0
	if  filter (Primes.isPrimeLookup, [o -(2*(x*x)) for x in xrange(1, int(o**0.5))]) ==[]:
		print o
		break
	
			
print time.time() - st, ' seconds'
