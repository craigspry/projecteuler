import Primes
import time
st = time.time()
primeList = filter(Primes.isPrimeLookup, range(2,1000))
primeList.sort();

def factors(n):
    facts = []
    if n==1:
        return facts
    if Primes.isPrimeLookup(n):
        facts.append(n)
        return facts
    for i in primeList:
        if n%i == 0:
            if n/i != n:
                facts.append(i)
                facts.extend(factors(n/i))
                return facts
            else:
                facts.extend(factors(i))
    return facts

def nextFact(start=2, end=1000000, numfactors=4):
	for i in range(start, end):
		if len(set(factors(i)))==numfactors:
			yield i
	
factList = []
nf = nextFact(numfactors=4)
i = nf.next()
factList.append(i)
while True:
	i = nf.next()
	if len(factList) > 3:
		lst = factList[-3:]
		if i -lst[2] == 1 and lst[2] - lst[1] == 1 and lst[1] - lst[0] == 1:
			lst.append(i)
			print lst
			break
	factList.append(i)
print time.time() - st
