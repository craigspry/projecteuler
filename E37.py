import time
import math
from itertools import count, imap
import Primes

def checkNum(num):
	if num < 9:
		return False
	i=0
	s=str(num)
	while i < len(s):
		if not Primes.isPrimeLookup(int(s[i:])):
			return False
		if i != 0 and not Primes.isPrimeLookup(int(s[:i*-1])):
			return False
		i += 1
	print num
	return True
	
st = time.time()
arr = []
i=1
while len(arr) < 11:
	if Primes.isPrimeLookup(i) and checkNum(i):
		arr.append(i)
	i+=2
print arr
print sum(arr)
print time.time() - st



