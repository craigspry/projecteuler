
def wordSum(s):
    sum = 0
    for c in s:
        sum += ord(c) - 64
    return sum

   
arr = set((i *(i+1))/2 for i in range(1,1000000))

str = ''
with open('words.txt', 'r') as f:
    str = f.read()

str = str.replace('"', '')
words = str.split(',')

triWords = []
for word in words:
    if wordSum(word) in arr:
        triWords.append(word)
print len(triWords)


 