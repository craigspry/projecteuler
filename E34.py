import time
import math

t = time.time()
arr = [math.factorial(x) for x in range(0,10)]
lookup = dict( zip((str(a) for a in range(0,10)),arr))
print sum((i for i in range(3,2540160) if sum (( lookup[x] for x in str(i))) == i))
print time.time() - t, ' seconds'
